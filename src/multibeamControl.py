#!/usr/bin/env python

# Basic ROS imports
import roslib 
roslib.load_manifest('nonius_boat')
import rospy
import PyKDL
import sys
import math as m
from scipy.interpolate import griddata

import matplotlib.pyplot as plt


import ctypes
import serial
import time
import os



from std_msgs.msg import Float64MultiArray 
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import NavSatFix
from geometry_msgs.msg import Pose 

from std_srvs.srv import Empty


# More imports
from numpy import *
import numpy
import tf

rospy.init_node("multibeam_boat")


class MultibeamBoat:
	#def getConfig(self):
		#self.topic_gps = rospy.get

	def getGPS(self, msgGPS):
		self.gpsPoint = msgGPS

		self.gpsPoint.latitude = 50 + (self.gpsPoint.latitude / (self.kLat + 1e-6) )
		self.gpsPoint.longitude = 35 + (self.gpsPoint.longitude / (self.kLon + 1e-6) )
		
		
		
	def getPose(self, poseMsg):
		self.currentPose = poseMsg
		self.alpha = m.acos(self.currentPose.orientation.w)*2  * sign(self.currentPose.orientation.z) * (-1)


	def getMultibeam(self, msg):
		ranges = array(msg.ranges)
		angle = msg.angle_min
		anglemax = msg.angle_max
		angleInc = msg.angle_increment
		height = ranges[(size(ranges)-1)/2]
		globRange = [0, 0]
		
		if (fabs(height) < 0.8) : return
		for length in range(size(ranges)-1):
			lengthray = ranges[length] * m.cos(angle)
			if not(math.isnan(lengthray)):
				globRange[0] = self.currentPose.position.x + (-1)*(height*m.tan(angle)) * m.cos(self.alpha)
				globRange[1] = self.currentPose.position.y + (-1)*(height*m.tan(angle)) * m.sin(self.alpha)
				self.valuesRange.append(lengthray)
				self.outData.append(globRange)
				#print globRange, self.valuesRange[size(self.valuesRange)-1], angle,  '\n'
				#print size(ranges)
				for item in globRange:
					self.log.write(str(item) + ';')
				self.log.write(str(ranges[length] * m.cos(angle)) + ';' + '\n')
				angle += angleInc
	#	We can publish globRange each getMultibeam, outData and gridData when finished
			
			
	def calculateMap(self, req):
		self.log.close()
		#print "calculateMap!!!!!!!!!!!!!"
		#maxX = self.outData[0][0]
		#minX = self.outData[0][0]
		#maxY = self.outData[0][1]
		#minY = self.outData[0][1]
		
		#for elem in self.outData:
		#	if maxX < elem[0] : maxX = elem[0]
		#	if minX > elem[0] : minX = elem[0]
		#	if maxY < elem[1] : maxY = elem[1]
		#	if minY > elem[1] : minY = elem[1]
			
		
		#myarray = numpy.asarray(self.outData)
		#myarray2 = numpy.asarray(self.valuesRange)
		
		#grid_x, grid_y = numpy.mgrid[minX:maxX:100j, minY:maxY:100j]		
		#gridOutData = griddata(myarray, myarray2, (grid_x, grid_y), method='linear')
		
		#for row in gridOutData:
		#	for elem in row:
		#		self.log2.write(str(elem) + ';')
		#	self.log.write('\n')
		#self.log2.close()
		

		#plt.imshow(gridOutData.T, extent=(minX,maxX,minY,maxY), origin='lower')
		#plt.title('Linear')
		#plt.show()
		#print "calculateMap!!!!!!!!!!!!!"
		return[]
		
		



	def __init__(self):
		self.namespace = sys.argv[1]
		self.vehicle_name = self.namespace
		self.gps_topic = sys.argv[2]
		self.pose_topic = sys.argv[3]
		self.multibeam_topic = sys.argv[4]
		
		self.outData = []
		self.valuesRange = []
		self.currentPose = Pose()
		self.alpha = 0
		
		#TODO remove this shit
		self.log = open('/home/ilys/log.csv', 'w')
		self.log2 = open('/home/ilys/logInterp.csv', 'w')
		rErth = 40040
		self.kLon = m.pi * rErth * m.cos(m.radians(50)) * 1000 / 180
		self.kLat = m.pi * rErth * 1000 / 180
		
	#	TODO: Need service for start GPS point, and for flag when ROV finish its work.
		rospy.Service('/dynamics/finishedTheTrack', Empty, self.calculateMap)
		
		
	#	Publish 
	#	self.multibeemDataPublisher= rospy.Publisher(self., )
        
	#   	Create Subscribers
		rospy.Subscriber(self.pose_topic, Pose, self.getPose)
		rospy.Subscriber(self.gps_topic, NavSatFix, self.getGPS)
		rospy.Subscriber(self.multibeam_topic, LaserScan, self.getMultibeam)


	def iterate(self):
		t1 = rospy.Time.now()
		
	# 	Main loop operations	
		
		t2 = rospy.Time.now()
		p = 0.001 - (t2-t1).to_sec()
		if p < 0.0 : p = 0.0
		rospy.sleep(p)

if __name__ == '__main__':
	try:
		mbb = MultibeamBoat() 
		rospy.sleep(5)
		while not rospy.is_shutdown():
			mbb.iterate()

	except rospy.ROSInterruptException: pass
		











