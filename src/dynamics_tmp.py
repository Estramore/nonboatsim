#!/usr/bin/env python

# Basic ROS imports
import roslib 
roslib.load_manifest('nonius_boat')
import rospy
import PyKDL
import sys
import math as m
import random as r

# import msgs
from std_msgs.msg import Float64MultiArray 
from geometry_msgs.msg import Pose 
from geometry_msgs.msg import Quaternion
from geometry_msgs.msg import WrenchStamped

#import services
from std_srvs.srv import Empty

# More imports
from numpy import *
import tf

class Dynamics :
	
	def getConfig(self) :
		self.mass = rospy.get_param(self.vehicle_name + "/dynamics" + "/mass")
		self.motors_distance = rospy.get_param(self.vehicle_name + "/dynamics" + "/motors_distance")
		
		self.initial_velocity = array(rospy.get_param(self.vehicle_name + "/dynamics" + "/initial_velocity"))
		self.initial_acceleration = array(rospy.get_param(self.vehicle_name + "/dynamics" + "/initial_acceleration"))
		self.initial_pose = array(rospy.get_param(self.vehicle_name + "/dynamics" + "/initial_pose"))
		
		self.period = rospy.get_param(self.vehicle_name + "/dynamics/period")
		self.uwsim_period = rospy.get_param(self.vehicle_name + "/dynamics/uwsim_period")
        
	
    	def pubPose(self, event):
        	pose = Pose()

        	position_tmp = self.position

        	pose.position.x = position_tmp[0]
        	pose.position.y = position_tmp[1]
        	pose.position.z = 0 

        	pose.orientation.x = 0
        	pose.orientation.y = 0
        	pose.orientation.z = -1 * m.sin(self.alpha/2)
        	pose.orientation.w = m.cos(self.alpha/2)

       		self.pub_pose.publish(pose)
        
	def mainLoop(self):
	#	Vector with the first force motor
		F1 = [self.enginesPower[0] * m.cos(self.alpha), self.enginesPower[0] * (-1) * m.sin(self.alpha)]
    	
	#	Vector with the second force motor	
		F2 = [self.enginesPower[1] * m.cos(self.alpha), self.enginesPower[1] * (-1) * m.sin(self.alpha)]
		
	#	Vector of wind
		windAngle = r.uniform(0.6, 0.8)
		windPower = r.uniform(0.8, 1.2)
		windSpeed = [windPower * m.cos(windAngle) / 100, windPower * (-1) * m.sin(windAngle) / 100]
		
	#	Vector of flow
		flowAngle = r.uniform(3.8, 4.2)
		flowPower = r.uniform(0.8, 1.2)*2
		flowSpeed = [flowPower * m.cos(flowAngle) / self.mass, flowPower * (-1) * m.sin(flowAngle) / self.mass]
	
	#	Acceleration, onli motors force TODO: add other forces
		acceleration = [(F1[0] + F2[0])/self.mass, (F1[1] + F2[1])/self.mass]
		
	#	Traveling speed
		self.resistKoeff = 1 + m.fabs(50*m.sin(self.alpha - m.atan(self.speed[1]/(self.speed[0]+1e-6))))
		
		resistForce = [	sign(self.speed[0]) * self.resistKoeff * m.pow(self.speed[0], 2) * m.pow(self.mass, 2/3) / self.mass, 
				sign(self.speed[1]) * self.resistKoeff * m.pow(self.speed[1], 2) * m.pow(self.mass, 2/3) / self.mass]

		self.speed[0] = (acceleration[0] - resistForce[0] + windSpeed[0])* self.period  + self.speed[0] 
		self.speed[1] = (acceleration[1] - resistForce[1] + windSpeed[1])* self.period  + self.speed[1] 
		
		#self.speed[0] = (acceleration[0] - resistForce[0]) * self.period + self.speed[0] 
		#self.speed[1] = (acceleration[1] - resistForce[1]) * self.period + self.speed[1] 
		#print self.speed, flowSpeed

		
	#	New position
		self.position[0] = self.position[0] + (self.speed[0] + flowSpeed[0])*self.period
		self.position[1] = self.position[1] + (self.speed[1] + flowSpeed[1])*self.period
		
	#	Acceleration rotation
		rotAcc = (self.enginesPower[0] - self.enginesPower[1])*(-2)/self.mass
		
	#	Speed rotation
		rotResist = sign(self.rotSpeed) * 0.05 * m.pow(self.rotSpeed, 2) * m.pow(self.mass, 2/3)
		self.rotSpeed += rotAcc * self.period - rotResist
		
	#	We think the angle of rotation 
		deltaAlpha = m.atan(self.rotSpeed * self.period/self.motors_distance)
		self.alpha = m.fmod((deltaAlpha + self.alpha), m.pi*2)
		
	def reset(self,req):
		self.speed = array(zeros(2))
		self.position = self.initial_pose
		return []
	
	def updateThrusters(self, thrusters):
		self.enginesPower = array(thrusters.data)
	
	
	def __init__(self):
		if len(sys.argv) != 6: 
			sys.exit("Usage: "+sys.argv[0]+" <namespace> <input_topic> <output_topic>")
        
		self.namespace=sys.argv[1]
		self.vehicle_name=self.namespace
		self.input_topic=sys.argv[2]
		self.pose_topic=sys.argv[3]
        
	#   	Load dynamic parameters
		self.getConfig()

		self.position = self.initial_pose
		self.alpha = m.acos(self.position[6])
		self.enginesPower = [0.0, 0.0]
		self.speed = array(zeros(2))
		self.rotSpeed = 0
		self.resistKoeff = 1
        
	#   	Create publisher
		self.pub_pose= rospy.Publisher(self.pose_topic, Pose, queue_size=10)
		rospy.init_node("dynamics_"+self.vehicle_name)
        
	#	Publish pose to UWSim
		rospy.Timer(rospy.Duration(self.uwsim_period), self.pubPose)
        
	#   	Create Subscribers for thrusters
		rospy.Subscriber(self.input_topic, Float64MultiArray, self.updateThrusters)

		s = rospy.Service('/dynamics/reset',Empty, self.reset)
        
	def iterate(self):
		t1 = rospy.Time.now()
		#print "ITERATE"

	# 	Main loop operations
		self.mainLoop()

		t2 = rospy.Time.now()
		p = self.period - (t2-t1).to_sec()
		if p < 0.0 : p = 0.0
		rospy.sleep(p)
        

if __name__ == '__main__':
	try:
		dynamics = Dynamics() 
		rospy.sleep(10)
		while not rospy.is_shutdown():
			dynamics.iterate()

	except rospy.ROSInterruptException: pass
    
	
	
	
