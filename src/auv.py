#!/usr/bin/env python

# Basic ROS imports
import roslib
import rospy
import PyKDL
import sys
import math as m

from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import NavSatFix
from geometry_msgs.msg import Pose

from std_srvs.srv import Empty

# More imports
from numpy import *
import tf

roslib.load_manifest('nonius_boat')

rospy.init_node("AUV_boat")

class AUV:
    def getConfig(self):
        self.initial_pose = array(rospy.get_param(self.vehicle_name + "/dynamics" + "/initial_pose"))
        self.period = rospy.get_param(self.vehicle_name + "/dynamics/period")
        self.uwsim_period = rospy.get_param(self.vehicle_name + "/dynamics/uwsim_period")

        #	coefficients for AUV
        self.Kp = rospy.get_param(self.vehicle_name + "/dynamics/Kp")
        self.Ki = rospy.get_param(self.vehicle_name + "/dynamics/Ki")
        self.Kd = rospy.get_param(self.vehicle_name + "/dynamics/Kd")
        self.deltaEps = rospy.get_param(self.vehicle_name + "/dynamics/delatEps")

        #	Gps point where simulation started
        self.startGPSPoint = NavSatFix()
        self.startGPSPoint.latitude = rospy.get_param(self.vehicle_name + "/dynamics/startGPS_lat")
        self.startGPSPoint.longitude = rospy.get_param(self.vehicle_name + "/dynamics/startGPS_long")

        #	GPS to meters
        rErth = 40040
        self.kLon = m.pi * rErth * m.cos(m.radians(self.startGPSPoint.latitude)) * 1000 / 180
        self.kLat = m.pi * rErth * 1000 / 180

    def poseGain(self, msg):
        self.pose = msg
        self.alpha = m.acos(self.pose.orientation.w) * 2 * sign(self.pose.orientation.z) * (-1)

    def gpsGain(self, msgGPS):
        self.gpsPoint.latitude = msgGPS.latitude + 0
        self.gpsPoint.longitude = msgGPS.longitude + 0

        self.xyPosition[0] = self.gpsPoint.latitude  # + self.startGPSPoint.latitude
        self.xyPosition[1] = self.gpsPoint.longitude  # + self.startGPSPoint.longitude

        self.gpsPoint.latitude = self.startGPSPoint.latitude + (self.gpsPoint.latitude / (self.kLat + 1e-6))
        self.gpsPoint.longitude = self.startGPSPoint.longitude + (self.gpsPoint.longitude / (self.kLon + 1e-6))

    def directionControl(self):

        for thisPoints in self.points:
            p = thisPoints[:]
            alphaNeed = 0

            p[0] = (p[0] - self.startGPSPoint.latitude) * self.kLat
            p[1] = (p[1] - self.startGPSPoint.longitude) * self.kLon

            print p

            errSum = 0
            deltaF = 0
            while (m.pow(self.xyPosition[0] - p[0], 2) + m.pow(self.xyPosition[1] - p[1], 2) > m.pow(self.deltaEps, 1)):
                t1 = rospy.Time.now()
                dirNeed = [p[0] - self.xyPosition[0], p[1] - self.xyPosition[1]]

                if dirNeed[0] > 0:
                    alphaNeed = -1 * m.asin(dirNeed[1] / self.modV(dirNeed[1], dirNeed[0]))
                else:
                    alphaNeed = m.pi + m.asin(dirNeed[1] / self.modV(dirNeed[1], dirNeed[0]))

                needAlpha = (self.alpha - alphaNeed)

                while needAlpha > m.pi:
                    needAlpha -= 2 * m.pi
                while needAlpha < -m.pi:
                    needAlpha += 2 * m.pi

                err = needAlpha
                errSum += err
                deltaF = self.Kp * err + self.Ki * errSum * self.period + self.Kd * err / self.period

                self.motPower[0] = 2 + deltaF
                self.motPower[1] = 2 - deltaF

                if m.fabs(self.motPower[0]) > 5:
                    self.motPower[0] = 5 * sign(self.motPower[0])
                if m.fabs(self.motPower[1]) > 5:
                    self.motPower[1] = 5 * sign(self.motPower[1])

                t2 = rospy.Time.now()
                slepTime = self.period - (t2 - t1).to_sec()
                if slepTime < 0.0: slepTime = 0.0
                rospy.sleep(slepTime)
            print "!!!!!!!!!!!!!!!! point taken !!!!!!!!"
            print self.xyPosition[0], self.xyPosition[1]
        self.finished()

    def modV(self, x, y):
        return m.sqrt(m.pow(x, 2) + m.pow(y, 2))

    def fillPoints(self):

        p = [[self.startGPSPoint.latitude, self.startGPSPoint.longitude + 0.00003],
             [self.startGPSPoint.latitude + 0.00001, self.startGPSPoint.longitude + 0.00003],
             [self.startGPSPoint.latitude + 0.00001, self.startGPSPoint.longitude],
             [self.startGPSPoint.latitude + 0.00002, self.startGPSPoint.longitude],
             [self.startGPSPoint.latitude + 0.00002, self.startGPSPoint.longitude + 0.00003],
             [self.startGPSPoint.latitude + 0.00003, self.startGPSPoint.longitude + 0.00003],
             [self.startGPSPoint.latitude + 0.00003, self.startGPSPoint.longitude],
             [self.startGPSPoint.latitude + 0.00003, self.startGPSPoint.longitude - 0.00001],
             [self.startGPSPoint.latitude - 0.00001, self.startGPSPoint.longitude - 0.00001],
             [self.startGPSPoint.latitude, self.startGPSPoint.longitude]]

        # self.points[0] = [self.startGPSPoint.latitude, self.startGPSPoint.longitude + 0.00001]
        # self.points[1] = [self.startGPSPoint.latitude + 0.00001, self.startGPSPoint.longitude + 0.00001]
        # self.points[2] = [self.startGPSPoint.latitude + 0.00001, self.startGPSPoint.longitude]
        # self.points[3] = [self.startGPSPoint.latitude, self.startGPSPoint.longitude]
        # self.points[4] = [self.startGPSPoint.latitude, self.startGPSPoint.longitude]
        self.points = p

    # print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    # print self.startGPSPoint.latitude, self.startGPSPoint.longitude

    def pubMotPower(self, event):
        msg = Float64MultiArray()
        msg.data = self.motPower
        self.pub_motors.publish(msg)

    def pubGPS(self, event):
        self.pub_true_gps.publish(self.gpsPoint)

    def __init__(self):

        self.vehicle_name = sys.argv[1]
        self.gpsPoint = NavSatFix()

        #   Load configuration
        self.getConfig()

        #   Add array with points
        self.points = array(zeros([5, 2]))
        self.fillPoints()

        self.pose = Pose()
        self.alpha = 0
        self.xyPosition = array(zeros(2))
        self.direction = array(zeros(2))

        #	Motors power, neuton
        self.motPower = array(zeros(2))

        #	AUV motor control
        self.pub_motors = rospy.Publisher("/boat/thrusters_input", Float64MultiArray, queue_size=10)
        rospy.Timer(rospy.Duration(self.uwsim_period), self.pubMotPower)

        self.pub_true_gps = rospy.Publisher("/boat/TRUE_GPS", NavSatFix, queue_size=10)
        rospy.Timer(rospy.Duration(1), self.pubGPS)

        #	Get GPS coordinates from uwsim
        rospy.Subscriber("/boat/gps", NavSatFix, self.gpsGain)
        rospy.Subscriber("/boat/pose", Pose, self.poseGain)

        rospy.wait_for_service('/dynamics/finishedTheTrack')
        self.finished = rospy.ServiceProxy('/dynamics/finishedTheTrack', Empty)

    def iterate(self):
        t1 = rospy.Time.now()

        # 	Main loop operations
        self.directionControl()

        t2 = rospy.Time.now()
        p = self.period - (t2 - t1).to_sec()
        if p < 0.0: p = 0.0
        rospy.sleep(p)


if __name__ == '__main__':
    try:
        auv = AUV()
        rospy.sleep(5)
        while not rospy.is_shutdown():
            auv.iterate()

    except rospy.ROSInterruptException:
        pass
